import React, { useState, useEffect } from 'react';
import { Button, StyleSheet, View, Text, ScrollView, SafeAreaView, TouchableOpacity, ImageBackground} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { WebView } from 'react-native-webview';

const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();

import chants from './data/data.json';

const chantsList = (navigation) => {
  return (
    chants.map((chant, index) => (
      <TouchableOpacity activeOpacity={0.6} onPress={() => { navigation.push('Chants', {...chant, index})}}>
        <Text key={index} style={styles.text}>{chant.name}</Text>
      </TouchableOpacity>)) 
  );
}

const chantsListFavorit = (navigation) => {

  return chants.map((chant, index) => {
    if (chant.favorites) {
      return (
        <TouchableOpacity activeOpacity={0.6} onPress={() => {  navigation.push('FavoritChants', {...chant, index})}}>
          <Text key={index} style={styles.text}>{chant.name}</Text>
        </TouchableOpacity>
      )
    }
  })
}
  

//const image = { uri: './assets/img/logo-og.png' };
const image = require('./assets/img/image.png')

function HomeScreen({ navigation }) {
  const [isShowingText, setIsShowingText] = useState(false);

  return (
    <View style={{flex: 1}}>
      <ImageBackground source={image} resizeMode="cover" style={styles.image}>
        <TouchableOpacity onPress={() => setIsShowingText(!isShowingText)} style={{ backgroundColor: "#42A55E", alignItems: 'center'}}><Text style={{color: '#fff'}}>Policy</Text>

        </TouchableOpacity>
        {isShowingText ? <WebView source={{ uri: 'https://google.com' }} /> : <View><Text></Text></View>}
          
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <ScrollView>
            {chantsList(navigation)}
          </ScrollView>
        </View>
        <View style={{ flexDirection: "row", backgroundColor: "#42A55E"}}>
          <TouchableOpacity style={styles.navigation}  onPress={() => navigation.navigate('Home')} >
            <Text style={styles.textButton}>Все</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.navigation}  onPress={() => navigation.navigate('Details')} >
            <Text style={styles.textButton2}>Избранные</Text>
          </TouchableOpacity>
        </View>
      </ImageBackground>
    </View>
  );
}
 



function ChantsScreen({navigation, route}) {

  const chant = route.params
  console.log(!chants[chant.index].favorites)
  return (
    <View style={{flex: 1}}>
      <ImageBackground source={image} resizeMode="cover" style={styles.image}>
        <SafeAreaView style={{flex: 1, alignItems: 'center', }}>
          <ScrollView style={{flex: 1}}>
            <Text style={styles.text}>{chant.description}</Text>
          </ScrollView>
        </SafeAreaView>
        <View style={{ flexDirection: "row", backgroundColor: "#42A55E"}}>
          <TouchableOpacity style={styles.navigation}  onPress={() => navigation.navigate('Home')} >
            <Text style={styles.textButton}>Назад</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.navigation}  onPress={() => {chants[chant.index].favorites = !chants[chant.index].favorites; navigation.navigate('Details')} } >
            <Text style={styles.textButton2}>Добавить в избранное</Text>
          </TouchableOpacity>
        </View>
      </ImageBackground>
    </View> 
  );
}


function FavoritChantsScreen({navigation, route}) {

  const chant = route.params
  return (
    <View style={{flex: 1}}>
      <ImageBackground source={image} resizeMode="cover" style={styles.image}>
        <SafeAreaView style={{flex: 1, alignItems: 'center', }}>
          <ScrollView style={{flex: 1}}>
            <Text style={styles.text}>{chant.description}</Text>
          </ScrollView>
        </SafeAreaView>
        <View style={{ flexDirection: "row", backgroundColor: "#42A55E"}}>
          <TouchableOpacity style={styles.navigation}  onPress={() => navigation.navigate('Home')} >
            <Text style={styles.textButton}>Назад</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.navigation}  onPress={() => {chants[chant.index].favorites = !chants[chant.index].favorites } }>
            <Text style={styles.textButton2}>Удалить с избранного</Text>
          </TouchableOpacity>
        </View>
      </ImageBackground>
    </View> 
  );
}

function SettingsScreen({ navigation, route}) {
  const chant = route.params
  return (
    <View style={{flex: 1}}>
      <ImageBackground source={image} resizeMode="cover" style={styles.image}>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Text>SettingsScreen!</Text>
          {chantsListFavorit(navigation)}
        </View>
        <View style={{ flexDirection: "row", backgroundColor: "#42A55E"}}>
          <TouchableOpacity style={styles.navigation}  onPress={() => navigation.navigate('Home')} >
            <Text style={styles.textButton}>Все</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.navigation}  onPress={() => navigation.navigate('Details')} >
            <Text style={styles.textButton}>Избранное</Text>
          </TouchableOpacity>
        </View>
      </ImageBackground>
    </View>
  );
}

export default function App() {
  return (
    <NavigationContainer style={{}}>
      <Stack.Navigator initialRouteName="Home" screenOptions={{headerShown: false }}>
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Details" component={SettingsScreen} />
        <Stack.Screen name="Chants" component={ChantsScreen} />
        <Stack.Screen name="FavoritChants" component={FavoritChantsScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};


const styles = StyleSheet.create({
  container: {
   flex: 1,
   justifyContent: 'center',
  },
  buttonContainer: {
    margin: 20
  },
  alternativeLayoutButtonContainer: {
    margin: 20,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  image: {
    flex: 1,
    justifyContent: "center"
  },
  textButton: {
    color: '#fff',
    fontSize: 22
  },

  textButton2: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 22,
    opacity: 0.5
  },

  text: {
    textAlign: "left",
    color: "white",
    fontSize: 34,
    backgroundColor: "#000000a1",
    margin: 8,
    width: 316,
    paddingHorizontal: 20, 
    paddingVertical: 10,
    borderRadius: 25,  
  },


  navigation: {
    flex: 1, 
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    minHeight: '12%',
  }
});